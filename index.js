const express = require("express");
const router = express.Router();
const initApi = require('./src/api/api');
const errorHandler = require('./src/middlewares/error-handler/error-handler-middleware');

const app = express();

const port = 3000;

app.use(express.json());

app.use('/', initApi(router));

app.use(errorHandler);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };