class User {
    constructor({ userRepository }) {
        this._userRepository = userRepository;
    }

    getCountOfUsers() {
        return this._userRepository.getCount();
    }

    getUserById(id) {
        return this._userRepository.getById(id);
    }

    createNewUser(data) {
        const { balance = 0, ...rest } = data;
        return this._userRepository.create({balance, ...rest });
    }

    updateUserById(id, data) {
        return this._userRepository.updateById(id, data);
    }

}

module.exports = User;
