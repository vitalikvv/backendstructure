const userRepository = require('../database/repositories/user.repository')
const transactionRepository = require('../database/repositories/transaction.repository')
const eventRepository = require('../database/repositories/event.repository')
const oddsRepository = require('../database/repositories/odds.repository')
const betsRepository = require('../database/repositories/bets.repository')
const User = require('./user/user.service');
const Transaction = require('./transaction/transaction.service');
const Event = require('./event/event.service');
const Odds = require('./odds/odds.service');
const Bets = require('./bets/bets.service');

module.exports = {

    transaction: new Transaction({
        transactionRepository,
        userRepository
    }),

    user: new User({
        userRepository
    }),

    event: new Event({
        eventRepository,
        oddsRepository,
        betsRepository,
        userRepository,
    }),

    odds: new Odds({
        oddsRepository
    }),

    bets: new Bets({
        betsRepository,
        userRepository,
        eventRepository,
        oddsRepository
    }),

}

