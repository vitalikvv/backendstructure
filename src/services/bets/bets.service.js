const createError = require('http-errors');

class Bets {
    constructor({ betsRepository, userRepository, eventRepository, oddsRepository }) {
        this._betsRepository = betsRepository;
        this._userRepository = userRepository;
        this._eventRepository = eventRepository;
        this._oddsRepository = oddsRepository;
    }

    getCountOfBets() {
        return this._betsRepository.getCount();
    }

    async createNewBets(data, userId) {
        const {eventId, betAmount, prediction} = data
        const event_id = eventId;
        const bet_amount = betAmount;
        const [user] = await this._userRepository.getById(userId);
        if(!user) {
            throw createError(400, 'User does not exist')
        }
        if(+user.balance < +bet_amount) {
            throw createError(400, 'Not enough balance')
        }
        const [event] = await this._eventRepository.getById(eventId)
        if(!event) {
            throw createError(404, 'Event not found')
        }
        const [odds] = await this._oddsRepository.getById(event.odds_id)
        if(!odds) {
            throw createError(404, 'Odds not found')
        }
        let multiplier;
        switch(prediction) {
            case 'w1':
                multiplier = odds.home_win;
                break;
            case 'w2':
                multiplier = odds.away_win;
                break;
            case 'x':
                multiplier = odds.draw;
                break;
        }
        const [newBet] = await this._betsRepository.create({
            prediction,
            event_id,
            bet_amount,
            user_id: userId,
            multiplier,
        });
        const currentBalance = user.balance - bet_amount;
        await this._userRepository.updateBalanceById(userId, currentBalance)
        return this._betsRepository.getById(newBet.id)
    }
}

module.exports = Bets;
