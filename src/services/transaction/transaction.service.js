const createError = require('http-errors');

class Transaction {
    constructor({ transactionRepository, userRepository}) {
        this._transactionRepository = transactionRepository;
        this._userRepository = userRepository;
    }

    getTransactionById(id) {
        return this._transactionRepository.getById(id);
    }

    async createNewTransaction(data) {
        const {userId, cardNumber, amount, ...rest} = data
        const user_id = userId;
        const card_number = cardNumber;
        const [user] = await this._userRepository.getById(userId);
        if(!user) {
            throw createError(400, 'User does not exist')
        }
        const newBalance = amount + user.balance;
        await this._userRepository.updateBalanceById(userId, newBalance);
        return this._transactionRepository.addNewTransaction({user_id, card_number, amount, ...rest});
    }

    updateUserById(id, data) {
        return this._transactionRepository.updateById(id, data);
    }

}

module.exports = Transaction;
