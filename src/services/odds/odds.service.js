const createError = require('http-errors');

class Odds {
    constructor({ oddsRepository}) {
        this._oddsRepository = oddsRepository;
    }

    getOddsById(id) {
        return this._oddsRepository.getById(id);
    }

}

module.exports = Odds;
