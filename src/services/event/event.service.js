class Event {
    constructor({ eventRepository, oddsRepository, betsRepository, userRepository }) {
        this._eventRepository = eventRepository;
        this._oddsRepository = oddsRepository;
        this._betsRepository = betsRepository;
        this._userRepository = userRepository;
    }

    getCountOfEvents() {
        return this._eventRepository.getCount();
    }

    async createNewEvent(data) {
        const {homeWin, awayWin, ...rest} = data.odds;
        const home_win = homeWin;
        const away_win = awayWin;
        const [newOdds] = await this._oddsRepository.create({home_win, away_win, ...rest})
        const {type, awayTeam, homeTeam, startAt} = data;
        const away_team = awayTeam;
        const home_team = homeTeam;
        const start_at = startAt;
        return await this._eventRepository.create({
            type,
            away_team,
            home_team,
            start_at,
            odds_id: newOdds.id
        })
    }

    async updateEventById(id, data) {
        const {score} = data;
        const bets = await this._betsRepository.getAllBetsByEventId(id)
        const [w1, w2] = score.split(":");
        let result;
        if(+w1 > +w2) {
            result = 'w1'
        } else if(+w2 > +w1) {
            result = 'w2';
        } else {
            result = 'x';
        }

        bets.map(async (bet) => {
            if(bet.prediction === result) {
                await this._betsRepository.updateById(bet.id, {win: true})
                const [user] = await this._userRepository.getById(bet.user_id)
                return await this._userRepository.updateById(bet.user_id, {
                    balance: user.balance + (bet.bet_amount * bet.multiplier),
                })
            } else if(bet.prediction !== result) {
                return await this._betsRepository.updateById(bet.id, {win: false})
            }
        });

        return this._eventRepository.updateById(id, data);
    }

}

module.exports = Event;
