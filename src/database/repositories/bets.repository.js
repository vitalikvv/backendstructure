const Base = require('./base-repository');
const db = require('../../../db/db')

class Bets extends Base {
    constructor({ betsModel }) {
        super(betsModel);
        this._betsModel = betsModel;
    }
    
    getAllBetsByEventId (id) {
        return db(this._betsModel).where('event_id', id).andWhere('win', null)
    }
}

const bets = new Bets({
    betsModel: 'bet'
});

module.exports = bets;
