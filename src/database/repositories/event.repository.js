const Base = require('./base-repository');
const db = require('../../../db/db')

class Event extends Base {
    constructor({ eventModel }) {
        super(eventModel);
        this._eventModel = eventModel;
    }
}

const event = new Event({
    eventModel: 'event'
});

module.exports = event;
