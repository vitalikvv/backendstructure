const Base = require('./base-repository');
const db = require('../../../db/db')

class Odds extends Base {
    constructor({ oddsModel }) {
        super(oddsModel);
        this._oddsModel = oddsModel;
    }
}

const odds = new Odds({
    oddsModel: 'odds'
});

module.exports = odds;
