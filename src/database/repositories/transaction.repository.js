const Base = require('./base-repository');
const db = require('../../../db/db')

class Transaction extends Base {
    constructor({ transactionModel }) {
        super(transactionModel);
        this._transactionModel = transactionModel;
    }

    addNewTransaction(data) {
        return db(this._transactionModel).insert(data).returning("*")
    }
}

const transaction = new Transaction({
    transactionModel: 'transaction'
});

module.exports = transaction;