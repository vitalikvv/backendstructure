const db = require('../../../db/db')

class Base {
    constructor(model) {
        this.model = model;
    }

    getCount() {
        return db(this.model).count('id', 'active')
    }

    getAll() {
        return;
    }

    getById(id) {
        return db(this.model).where('id', id).returning("*");
    }

    create(data) {
        return db(this.model).insert(data).returning("*")
    }

   updateById(id, data) {
        return db(this.model).where('id', id).update(data).returning("*");
    }

    deleteById(id) {
        return;
    }
}

module.exports = Base;
