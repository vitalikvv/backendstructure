const Base = require('./base-repository');
const db = require('../../../db/db')

class User extends Base {
    constructor({ userModel }) {
        super(userModel);
        this._userModel = userModel;
    }

    updateBalanceById(id, balance) {
        return db(this._userModel).where('id', id).update('balance', balance)
    }

}

const user = new User({
    userModel: 'user'
});

module.exports = user;
