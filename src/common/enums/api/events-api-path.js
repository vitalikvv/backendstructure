const EventsApiPath = {
    ROOT: '/',
    $ID: '/:id'
};

module.exports = EventsApiPath;