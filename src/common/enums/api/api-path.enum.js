const ApiPath = {
    BETS: '/bets',
    EVENTS: '/events',
    HEALTH: '/health',
    STATS: '/stats',
    TRANSACTIONS: '/transactions',
    USERS: '/users'
};

module.exports = ApiPath;