const UsersApiPath = {
    ROOT: '/',
    $ID: '/:id'
};

module.exports = UsersApiPath;