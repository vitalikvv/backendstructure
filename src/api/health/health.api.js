const ApiPath = require('../../common/enums/api/api-path.enum');
const HealthApiPath = require('../../common/enums/api/health-api-path');

const initHealth = (Router) => {
    const router = Router;

    router.get(HealthApiPath.ROOT, (req, res, next) => res.send("Hello World!"))
    return router;
};

module.exports = initHealth;
