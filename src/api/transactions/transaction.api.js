const TransactionsApiPath = require('../../common/enums/api/transactions-api-path');
const transactionValidator = require('../../middlewares/validation/transaction.validation');
const authorizationMiddleware = require('../../middlewares/authorization/autorization.middleware');
const createError = require('http-errors');

const initTransactions = (Router, services) => {
    const router = Router;
    const { transaction: transactionService } = services;
    router
        .post(TransactionsApiPath.ROOT, authorizationMiddleware, transactionValidator.createTransaction, (req, res, next) => transactionService
            .createNewTransaction(req.body)
            .then((data) => {
                res.send(data)
            })
            .catch(next))

    return router;
};

module.exports = initTransactions;
