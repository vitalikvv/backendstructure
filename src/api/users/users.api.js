const UsersApiPath = require('../../common/enums/api/users-api-path');
const userValidator = require('../../middlewares/validation/user.validation');
const authorizationMiddleware = require('../../middlewares/authorization/autorization.middleware');
const createError = require('http-errors');
const jwt = require("jsonwebtoken");
const ee = require('events');

const initUsers = (Router, services) => {
    const router = Router;
    const { user: userService } = services;
    const statEmitter = new ee();
    router
        .get(UsersApiPath.$ID, userValidator.findUserById, (req, res, next) => userService
            .getUserById(req.params.id)
            .then(([user]) => {
                if (!user) {
                    throw createError(404, 'User not found')
                }
                res.send({...user})
            })
            .catch(next))

        .post(UsersApiPath.ROOT, userValidator.createUser, (req, res, next) => userService
            .createNewUser(req.body)
            .then(([user]) => {
                statEmitter.emit('newUser');
                const {created_at, updated_at, ...rest} = user;
                const createdAt = created_at;
                const updatedAt = updated_at;
                res.send({
                    ...rest,
                    createdAt,
                    updatedAt,
                    accessToken: jwt.sign({ id: user.id, type: user.type }, process.env.JWT_SECRET)
                })
            })
            .catch(error => {
                if(error.code === '23505') {
                    throw createError(400, error.detail)
                }
            })
            .catch(next))

        .put(UsersApiPath.$ID, authorizationMiddleware, userValidator.updateUserById, (req, res, next) => userService
            .updateUserById(req.params.id, req.body)
            .then(([user]) => {
                if (!user) {
                    throw createError(404, 'User not found')
                }
                const {created_at, updated_at, ...rest} = user;
                const createdAt = created_at;
                const updatedAt = updated_at;
                res.send({
                    ...rest,
                    createdAt,
                    updatedAt,
                })
            })
            .catch(error => {
                if(error.code === '23505') {
                    throw createError(400, error.detail)
                }
            })
            .catch(next))

    return router;
};

module.exports = initUsers;
