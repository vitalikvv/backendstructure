const ApiPath = require('../common/enums/api/api-path.enum');
const services = require('../services/services')
const HealthApiPath = require('../common/enums/api/health-api-path');
const TransactionsApiPath = require('../common/enums/api/transactions-api-path');
const createError = require('http-errors');
const authorizationMiddleware = require('../middlewares/authorization/autorization.middleware');
const transactionValidator = require('../middlewares/validation/transaction.validation');
const jwt = require("jsonwebtoken");
const UsersApiPath = require('../common/enums/api/users-api-path');
const userValidator = require('../middlewares/validation/user.validation');
const EventsApiPath = require('../common/enums/api/events-api-path');
const StatsApiPath = require('../common/enums/api/stats-api-path');
const BetsApiPath = require('../common/enums/api/bets-api-path');
const eventValidator = require('../middlewares/validation/event.validation')
const betsValidator = require('../middlewares/validation/bets.validation')

// register all routes

const initApi = Router => {

    const router = Router;

    router
        .get(ApiPath.HEALTH + HealthApiPath.ROOT, (req, res, next) => res.send("Hello World!"))

        .post(ApiPath.TRANSACTIONS + TransactionsApiPath.ROOT, authorizationMiddleware, transactionValidator.createTransaction, (req, res, next) => {
            services.transaction.createNewTransaction(req.body)
                .then(([transaction]) => {
                    services.user.getUserById(transaction.user_id)
                        .then(([user]) => {
                            res.send({
                                id: transaction.id,
                                amount: transaction.amount,
                                userId: transaction.user_id,
                                cardNumber: transaction.card_number,
                                createdAt: transaction.created_at,
                                updatedAt: transaction.updated_at,
                                currentBalance: user.balance
                            })
                        })
                })
                .catch(next)
        })

        .get(ApiPath.USERS + UsersApiPath.$ID, userValidator.findUserById, (req, res, next) => services.user
            .getUserById(req.params.id)
            .then(([user]) => {
                if (!user) {
                    throw createError(404, 'User not found')
                }
                res.send({...user})
            })
            .catch(next))

        .post(ApiPath.USERS + UsersApiPath.ROOT, userValidator.createUser, (req, res, next) => services.user
            .createNewUser(req.body)
            .then(([user]) => {
                const {created_at, updated_at, ...rest} = user;
                const createdAt = created_at;
                const updatedAt = updated_at;
                res.send({
                    ...rest,
                    createdAt,
                    updatedAt,
                    accessToken: jwt.sign({id: user.id, type: user.type}, process.env.JWT_SECRET)
                })
            })
            .catch(error => {
                if (error.code === '23505') {
                    throw createError(400, error.detail)
                }
            })
            .catch(next))

        .put(ApiPath.USERS + UsersApiPath.$ID, authorizationMiddleware, userValidator.updateUserById, (req, res, next) => services.user
            .updateUserById(req.params.id, req.body)
            .then(([user]) => {
                if (!user) {
                    throw createError(404, 'User not found')
                }
                const {created_at, updated_at, ...rest} = user;
                const createdAt = created_at;
                const updatedAt = updated_at;
                res.send({
                    ...rest,
                    createdAt,
                    updatedAt,
                })
            })
            .catch(error => {
                if (error.code === '23505') {
                    throw createError(400, error.detail)
                }
            })
            .catch(next))

        .post(ApiPath.EVENTS + EventsApiPath.ROOT, authorizationMiddleware, eventValidator.createEvent, (req, res, next) => services.event
            .createNewEvent(req.body)
            .then(([event]) => {
                const {odds_id, home_team, away_team, start_at, created_at, updated_at, ...rest} = event;
                const awayTeam = away_team;
                const homeTeam = home_team;
                const oddsId = odds_id;
                const startAt = start_at;
                const updatedAt = updated_at;
                const createdAt = created_at;
                const eventToResponse = {...rest, awayTeam, homeTeam, oddsId, startAt, updatedAt, createdAt}
                services.odds.getOddsById(event.odds_id)
                    .then(([odds]) => {
                        const {home_win, away_win, created_at, updated_at, ...rest} = odds;
                        const homeWin = home_win;
                        const awayWin = away_win;
                        const updatedAt = updated_at;
                        const createdAt = created_at;
                        res.send({
                            ...eventToResponse,
                            odds: {...rest, homeWin, awayWin, updatedAt, createdAt}
                        })
                    })
            })
            .catch(next))

        .put(ApiPath.EVENTS + EventsApiPath.$ID, authorizationMiddleware, eventValidator.updateEventById, (req, res, next) => services.event
            .updateEventById(req.params.id, req.body)
            .then(([event]) => {
                const {odds_id, home_team, away_team, start_at, created_at, updated_at, ...rest} = event;
                const awayTeam = away_team;
                const homeTeam = home_team;
                const oddsId = odds_id;
                const startAt = start_at;
                const updatedAt = updated_at;
                const createdAt = created_at;
                res.send({
                    ...rest,
                    awayTeam,
                    homeTeam,
                    oddsId,
                    startAt,
                    updatedAt,
                    createdAt
                })
            })
            .catch(next))

        .post(ApiPath.BETS + BetsApiPath.ROOT, authorizationMiddleware, betsValidator.createBets, (req, res, next) => services.bets
            .createNewBets(req.body, res.locals.userId)
            .then(([bet]) => {
                const {event_id, user_id, bet_amount, created_at, updated_at, ...rest} = bet;
                const eventId = event_id;
                const userId = user_id;
                const betAmount = bet_amount;
                const updatedAt = updated_at;
                const createdAt = created_at;
                const betToResponse = {...rest, eventId, userId, betAmount, updatedAt, createdAt}
                services.user.getUserById(res.locals.userId)
                    .then(([user]) => {
                        res.send({
                            ...betToResponse,
                            currentBalance: user.balance
                        })
                    })
            })
            .catch(next))

        .get(ApiPath.STATS + StatsApiPath.ROOT, authorizationMiddleware, (req, res, next) => services.user
            .getCountOfUsers()
            .then(([countOfUsers]) => {
                services.event.getCountOfEvents()
                    .then(([countOfEvents]) => {
                        services.bets.getCountOfBets()
                            .then(([countOfBets]) => {
                                res.send({
                                    totalUsers: countOfUsers.count,
                                    totalBets: countOfBets.count,
                                    totalEvents: countOfEvents.count
                                })
                            })
                    })
            })
            .catch(next))

    return router;
};

module.exports = initApi;


