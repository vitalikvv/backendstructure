const errorHandler = (err, _req, res, next) => {

    if (res.headersSent) {
        next(err);
    } else {
        const { status = 500, message = 'Internal Server Error' } = err;
        res.status(status).send({ status, message });
    }
};

module.exports = errorHandler;
