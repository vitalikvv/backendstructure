const createError = require('http-errors');
const jwt = require("jsonwebtoken");
const routesForAdmin = require('../../common/constants/routesForAdmin');

const checkAccess = (url) => {
    const changedUrl = url.split('/')[1];
    return routesForAdmin.some(item => item === changedUrl)
}

const checkUrl = (url) => {
    const changedUrl = url.split('/')[1];
    return changedUrl === 'users'
}

const authorizationMiddleware = (req, res, next) => {
    let token = req.headers['authorization'];
    let tokenPayload;
    if(!token) {
        throw createError(401, 'Not Authorized')
    }
    token = token.replace('Bearer ', '');
    try {
        tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        res.locals.userId = tokenPayload.id;
    } catch (err) {
        throw createError(401, 'Not Authorized')
    }
    const isNeedAccessForAdmin = checkAccess(req.originalUrl)
    if (tokenPayload.type !== 'admin' && isNeedAccessForAdmin) {
        throw createError(401, 'You are not admin')
    }
    const isUpdateUser = checkUrl(req.originalUrl)
    if(isUpdateUser && req.params.id && req.params.id !== tokenPayload.id) {
        throw createError(401, 'UserId mismatch')
    }
    next();
}

module.exports = authorizationMiddleware;