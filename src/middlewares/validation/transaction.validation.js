const createError = require('http-errors');
const schemas = require('./validate-schemas/schemas')

module.exports = {
    createTransaction: (req, res, next) => {
        const isValidResult = schemas.createNewTransactionSchema.validate(req.body);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },
}
