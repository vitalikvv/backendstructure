const createError = require('http-errors');
const schemas = require('./validate-schemas/schemas')

module.exports = {
    createBets: (req, res, next) => {
        const isValidResult = schemas.createNewBetsSchema.validate(req.body);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },
}
