const createError = require('http-errors');
const schemas = require('./validate-schemas/schemas')

module.exports = {
    createEvent: (req, res, next) => {
        const isValidResult = schemas.createNewEventSchema.validate(req.body);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },

    updateEventById: (req, res, next) => {
        const isValidResult = schemas.updateEventByIdSchema.validate(req.body);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },
}
