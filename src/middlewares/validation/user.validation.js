const createError = require('http-errors');
const schemas = require('./validate-schemas/schemas')

module.exports = {
    findUserById: (req, res, next) => {
        const isValidResult = schemas.findUserByIdSchema.validate(req.params);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },

    createUser: (req, res, next) => {
        const isValidResult = schemas.createNewUserSchema.validate(req.body);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },

    updateUserById: (req, res, next) => {
        const isValidResult = schemas.updateUserByIdSchema.validate(req.body);
        if(isValidResult.error) {
            throw createError(400, isValidResult.error.details[0].message)
        }
        next();
    },
}
